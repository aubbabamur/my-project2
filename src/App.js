// import logo from './logo.svg';
import './App.css';

function Person(props) {
  return (
    
    <div className="person-card">
      <div className="title">
      {props.name}
      </div>
      <div className="age">
       {props.age}
      </div>
      <div className="src">
      <img src = { props.src } />
        </div> 
      <div className='link'>
      <a href= {props.link} target='_blank'>
       button
        </a>
      </div>
      </div>
        
     
      
      
      
 )}
    

    function App() {
      return (
        <div className='list-wrapper'>
        <Person name="Назад в будущее" age="3 июля 1985 г."  link='https://www.kinopoisk.ru/film/476/' src="https://avatars.mds.yandex.net/get-kinopoisk-image/1599028/73cf2ed0-fd52-47a2-9e26-74104360786a/600x900"/>
        <Person name="Форрест Гамп" age="6 июля 1994 г."  link='https://www.kinopoisk.ru/film/448/' src='https://upload.wikimedia.org/wikipedia/ru/d/de/%D0%A4%D0%BE%D1%80%D1%80%D0%B5%D1%81%D1%82_%D0%93%D0%B0%D0%BC%D0%BF.jpg'/>
        <Person name="Властелин колец" age="2001—2003 г." src='https://upload.wikimedia.org/wikipedia/ru/e/e6/%D0%9A%D0%B8%D0%BD%D0%BE%D1%82%D1%80%D0%B8%D0%BB%D0%BE%D0%B3%D0%B8%D1%8F_%D0%92%D0%BB%D0%B0%D1%81%D1%82%D0%B5%D0%BB%D0%B8%D0%BD_%D0%9A%D0%BE%D0%BB%D0%B5%D1%86.jpg' link='https://www.kinopoisk.ru/film/328/'/>
        {/* <Person name="John Smith" age="32" src='/image/shark.jpg' src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRlISdD99P3n4ns-BYO0E4GXET5k5A3DZAoRQ&usqp=CAU'/> */}
        </div>
  )
}

export default App;
